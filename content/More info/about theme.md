title: About put_it_out_there
date: 2021-02-01 14:15
status: published
tags: introduction, 100DaysToOffload
hundreddaystooffload: 10

## What

Clean and responsive theme for building a blog using [Pelican](https://blog.getpelican.com/ "Visit Pelican site") static site generator.

## Why

1. Many themes available for Pelican are outdated and don't look good on smaller screen devices.
2. Few good looking ones are too heavy in my opinion and integrates unnecessary stuff such as Google Analytics and Disqus for comments

## How

Using below resources:

1. [blueidea](https://github.com/nebulousdog/pelican-blueidea "GitHub repository") : Helped me to build the base template (mostly via copy pasting 😁)
2. [Simple.css {}](https://github.com/kevquirk/simple.css "GitHub repository") : This is the stylesheet that gives the responsiveness and clean look to the template.

## Who

You, me, everyone with access to internet

## Where

Wherever python3 works.

A smartphone? Download terminal and write in there 😜

Raspberry Pi? You can host your pelican site to hundreds of thousands of people every month with that thing and it probably will be fine.

300$ low-end laptop? This is overkill.

## When

Whenever you are ready.
