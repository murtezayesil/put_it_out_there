title: README
date: 2021-01-19
status: published
tags: introduction, must have
hundreddaystooffload: 

put_it_out_there is a light and responsive theme built with [Jinja](https://jinja.palletsprojects.com "Modern and Designer-friendly templating language for Python") and [simple.css](https://simplecss.org/ "A classless CSS framework to make a good looking site quickly") for [Pelican](https://blog.getpelican.com/ "Static Site Generator powered by Python").

## Goal

Host a __Light__, __Fast__ and __Responsive__ website for great visitor experience and reduced energy consumption.

### Light ☁️

put_it_out_there uses [simple.css](https://simplecss.org/) which is a tiny, ~7KB, CSS framework for replacing heavy CSS frameworks your current site maybe relying on. Bootstrap is a famous framework for building responsive websites. [Current version](https://github.com/twbs/bootstrap/blob/v5.0.0-beta1/dist/css/bootstrap.css "Bootstrap v5.0.0 beta1") weighs 188KB.

Lightness of put_it_out_there is important since it aims to offer a good experience regardless of how old the visitor's device is.

put_it_out_there includes one more CSS file for few tricks. But don't worry, that CSS file is even smaller than simple.css 🙂

### Fast ⚡

Light sites tend to load faster. This is crucial for good visitor experience since long load times can prompt visitors to leave your site.
Average site in 2019 took [longer than 1 second](https://backlinko.com/page-speed-stats "") to start loading and take almost half a minute to finish loading (on mobile devices). Sites using put_it_out_there can finish loading in first second.

### Responsive 💻📱

It looks good on both small and large screens.

# Side Features
1. No Tracking : There is no need to annoy visitors with cookie warnings 😃
2. [Static Site](https://en.wikipedia.org/wiki/Static_web_page "Learn more on Wikipedia") : An old computer or even a business card sized RaspberryPi can handle hosting without a sweat.
3. [#100DaysToOffload](https://100daystooffload.com/ "#100DaysToOffload Challenge") : Participate in 
4. WIP - Top Banner
5. WIP - Privacy respecting Analytics integration
