title: Preview all the things
date: 2021-02-05
Summary: Showing off what markdown features Pelican and put_it_out_there offer

Headings

# This is an <h1> tag
## This is an <h2> tag
### This is an <h3> tag
#### This is an <h4> tag
##### This is an <h5> tag
###### This is an <h6> tag

---

Paragraph

This is a paragraph with some [link to Wikipedia](https://wikipedia.org "Click to open Wikipedia"). This is __bold__ and this *italic*. You can also ___combine them___. There are options to write text in ~~strikethrough~~. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam maximus dolor et mauris dignissim, in porta leo ullamcorper. Ut quam orci, aliquet sed congue sagittis, consectetur a lectus. In pellentesque neque nec odio pulvinar, non molestie felis consequat.

Donec turpis mi, commodo id ligula non, egestas vulputate dui. Aenean efficitur semper velit id placerat. Nullam pellentesque rutrum lorem et convallis. Duis suscipit ac nisl vel dapibus. Aliquam eu imperdiet lorem. Suspendisse ultrices sodales erat. Fusce ac turpis quis ipsum suscipit egestas. Suspendisse blandit purus placerat, convallis turpis vitae, mollis enim. Morbi vel enim rutrum, ultricies nisi non, maximus tortor.

---

Emphasize

*This text will be italic*
_This will also be italic_

**This text will be bold**
__This will also be bold__

_You **can** combine them_ _**like this**_

---

Block quote 

> Don’t communicate by sharing memory, share memory by communicating.
> 
> Rob Pike during Gopherfest, 18th Nov 2015

Code

```python
print(Hello, World!)
```

Unordered list

* Cherry
* Banana
* Melon

Ordered List

1. Hat
2. Jacket
3. Umbrella

Nested List

### Shopping List
1. Fruit
    * [ ] pineapple
    * [ ] mango
    * [ ] jackfruit
2. Vegetable
    1. Tomato
    2. Potato
3. Dairy
    * Milk
    * Cheese

